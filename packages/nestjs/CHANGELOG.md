# @adaliszk/nestjs

## 9.3.6

### Patch Changes

- 74c239e: fix: resolve compilation errors

## 9.3.5

### Patch Changes

- 0286cc3: chore: Update dependenices to the latest versions

## 9.3.4

### Patch Changes

- dd8a611: Update NestJS stack

## 9.3.3

### Patch Changes

- aa506ec: Fix adapter and plugin usage

## 9.3.2

### Patch Changes

- 003f5eb: Update eslint rules

## 9.3.1

### Patch Changes

- 34841fc: Remove autmatic listener and allow users to listen

## 9.2.5

### Patch Changes

- 81e2574: Update eslint and pin dependencies to minor versions

## 9.2.4

### Patch Changes

- 6f25d61: Adding boostrap helpers

## 9.2.3

### Patch Changes

- eb77590: Adding Winston Logger module generator with a mixed console and file output

## 9.2.2

### Patch Changes

- 0385c09: release(nestjs): fix missed contents

## 9.2.1

### Patch Changes

- a82d348: release(nestjs): add POC bundle
