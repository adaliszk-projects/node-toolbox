# @adaliszk/nestjs-server-example

## 0.0.1

### Patch Changes

- 74c239e: fix: resolve compilation errors
- Updated dependencies [74c239e]
  - @adaliszk/nestjs-fastify@9.3.4
  - @adaliszk/nestjs@9.3.6

## null

### Patch Changes

- Updated dependencies [0286cc3]
  - @adaliszk/nestjs-fastify@9.3.3
  - @adaliszk/nestjs@9.3.5

## null

### Patch Changes

- Updated dependencies [dd8a611]
  - @adaliszk/nestjs@9.3.4
  - @adaliszk/nestjs-fastify@9.3.2
