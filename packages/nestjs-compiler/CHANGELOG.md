# @adaliszk/nestjs-compiler

## 9.3.4

### Patch Changes

- 75d5094: feat(nestjs-compiler): add configuration file support + set platfrom to node

## 9.3.3

### Patch Changes

- 74c239e: fix: resolve compilation errors

## 9.3.2

### Patch Changes

- 0286cc3: chore: Update dependenices to the latest versions

## 9.3.1

### Patch Changes

- dd8a611: Update NestJS stack
