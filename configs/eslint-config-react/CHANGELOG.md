# @adaliszk/eslint-config-react

## 18.2.16

### Patch Changes

- 0286cc3: chore: Update dependenices to the latest versions
- Updated dependencies [0286cc3]
  - @adaliszk/eslint-config-typescript@8.39.1

## 18.2.15

### Patch Changes

- 003f5eb: Update eslint rules
- Updated dependencies [003f5eb]
  - @adaliszk/eslint-config-typescript@8.34.3

## 18.2.14

### Patch Changes

- d56299d: Update react and base rules
- Updated dependencies [d56299d]
  - @adaliszk/eslint-config-typescript@8.34.2

## 18.2.13

### Patch Changes

- 0f17907: Update versions

## 18.2.12

### Patch Changes

- 00a6a2e: Update rules

## 18.2.11

### Patch Changes

- 81e2574: Update eslint and pin dependencies to minor versions
- Updated dependencies [81e2574]
  - @adaliszk/eslint-config-typescript@8.34.1

## 18.2.10

### Patch Changes

- 703f631: Rebuild distribution
- Updated dependencies [703f631]
  - @adaliszk/eslint-config-typescript@8.33.4

## 18.2.9

### Patch Changes

- cb9f458: Update dist files
- Updated dependencies [cb9f458]
  - @adaliszk/eslint-config-typescript@8.33.3

## 18.2.8

### Patch Changes

- 354355f: Depend on react for rules with Qwik

## 18.2.7

### Patch Changes

- a3cf3a2: Update to the latest changes

## 18.2.6

### Patch Changes

- 927c62d: Adding default configuration

## 18.2.5

### Patch Changes

- 2567578: Update rules and dependencies
- Updated dependencies [2567578]
  - @adaliszk/eslint-config-typescript@8.33.2

## 18.2.4

### Patch Changes

- 136be34: Forwarding Eslint peer dependency
